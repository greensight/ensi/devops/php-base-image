FROM php:8.0.3-fpm-alpine

ARG LIBRDKAFKA_VERSION=1.6.1
ARG PHP_RDKAFKA_VERSION=5.0.2

RUN apk add --virtual .build-deps --no-cache --update autoconf file g++ gcc libc-dev make pkgconf re2c zlib-dev bash git && \
    apk add --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted gnu-libiconv && \
    apk add --no-cache freetype-dev libjpeg-turbo-dev libwebp-dev libpng-dev libzip-dev postgresql-dev librdkafka-dev && \
    pecl install rdkafka-$PHP_RDKAFKA_VERSION && \
    pecl install apcu && \
    pecl install redis && \
    docker-php-ext-configure gd \
    --with-freetype=/usr/include/ \
    --with-webp=/usr/include/ \
    --with-jpeg=/usr/include/ && \
    docker-php-ext-install pdo_pgsql iconv gd zip opcache pcntl && \
    docker-php-ext-enable apcu pdo_pgsql redis rdkafka && \
    apk del -f .build-deps && \
    pecl clear cache \

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

COPY rootfs /

WORKDIR /var/www

ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["php-fpm"]