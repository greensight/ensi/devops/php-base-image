FROM dockerhub.greensight.ru/ensi-tech/php-base-image:8.0-2021apr19-1

RUN mkdir -p /var/log/newrelic /var/run/newrelic /usr/local/src && \
    touch /var/log/newrelic/php_agent.log /var/log/newrelic/newrelic-daemon.log && \
    chmod -R g+ws /usr/local/src /var/log/newrelic/ /var/run/newrelic/ && \
    chown -R 1001:0 /usr/local/src /var/log/newrelic/ /var/run/newrelic/ && \
    export NEWRELIC_VERSION=$(curl -sS https://download.newrelic.com/php_agent/release/ | sed -n 's/.*>\(.*linux-musl\).tar.gz<.*/\1/p') && \
    cd /usr/local/src && curl -sS "https://download.newrelic.com/php_agent/release/${NEWRELIC_VERSION}.tar.gz" | gzip -dc | tar xf - && \
    cd "${NEWRELIC_VERSION}" && \
    NR_INSTALL_SILENT=true ./newrelic-install install && \
    rm -f /var/run/newrelic-daemon.pid && \
    rm -f /tmp/.newrelic.sock