FROM php:8.4.3-fpm-alpine3.20

RUN apk add --virtual .build-deps --no-cache --update autoconf dpkg-dev dpkg file g++ gcc libc-dev make pkgconf re2c \
        zlib-dev=1.3.1-r1 freetype-dev=2.13.2-r0 libjpeg-turbo-dev=3.0.3-r0 libwebp-dev=1.3.2-r0 libpng-dev=1.6.44-r0 libzip-dev=1.10.1-r0 postgresql-dev librdkafka-dev=2.4.0-r0 \
        bash git ca-certificates && \
    apk add --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted \
        gnu-libiconv=1.17-r2 freetype=2.13.2-r0 libjpeg-turbo=3.0.3-r0 libwebp=1.3.2-r0 libpng=1.6.44-r0 libzip=1.10.1-r0 libpq=16.6-r0 librdkafka=2.4.0-r0 && \
    pecl install rdkafka-6.0.5 apcu-5.1.24 redis-6.1.0 swoole-6.0.0 && \
    docker-php-ext-configure gd \
        --with-freetype=/usr/include/ \
        --with-webp=/usr/include/ \
        --with-jpeg=/usr/include/ && \
    docker-php-ext-install pdo_pgsql gd zip opcache pcntl && \
    docker-php-ext-enable apcu pdo_pgsql redis rdkafka swoole && \
    apk del -f .build-deps && \
    pecl clear cache

COPY rootfs /

RUN cat /usr/local/share/ca-certificates/cert_ca_mintsifry.crt >> /etc/ssl/certs/ca-certificates.crt && \
    update-ca-certificates

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

WORKDIR /var/www
