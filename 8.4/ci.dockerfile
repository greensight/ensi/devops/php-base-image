ARG BASE_IMAGE

FROM $BASE_IMAGE
RUN apk add --no-cache --update git npm bash

RUN printf "# composer php cli ini settings\n\
date.timezone=UTC\n\
memory_limit=-1\n\
" > $PHP_INI_DIR/php-cli.ini

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp

COPY --from=composer:2.5 /usr/bin/composer /usr/bin/composer