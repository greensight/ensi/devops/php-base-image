FROM php:8.2.8-fpm-alpine3.18

ARG LIBRDKAFKA_VERSION=1.6.1
ARG PHP_RDKAFKA_VERSION=5.0.2

COPY CA/cert_ca_mintsifry.pem /usr/local/share/ca-certificates/cert_ca_mintsifry.crt

RUN apk add --virtual .build-deps --no-cache --update $PHPIZE_DEPS \
        zlib-dev freetype-dev libjpeg-turbo-dev libwebp-dev libpng-dev libzip-dev postgresql-dev librdkafka-dev \
        bash git ca-certificates && \
    apk add --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted \
        gnu-libiconv freetype libjpeg-turbo libwebp libpng libzip libpq librdkafka && \
    pecl install rdkafka-$PHP_RDKAFKA_VERSION apcu redis swoole && \
    docker-php-ext-configure gd \
        --with-freetype=/usr/include/ \
        --with-webp=/usr/include/ \
        --with-jpeg=/usr/include/ && \
    docker-php-ext-install pdo_pgsql gd zip opcache pcntl && \
    docker-php-ext-enable apcu pdo_pgsql redis rdkafka swoole && \
    cat /usr/local/share/ca-certificates/cert_ca_mintsifry.crt >> /etc/ssl/certs/ca-certificates.crt && \
    update-ca-certificates && \
    apk del -f .build-deps && \
    pecl clear cache

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

COPY rootfs /

WORKDIR /var/www

ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["php", "artisan", "octane:start", "--host=0.0.0.0", "--workers=10"]