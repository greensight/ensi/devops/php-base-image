#!groovy
// -*- coding: utf-8; mode: Groovy; -*-

// ===========================================================================//
// Параметры, которые надо переопределить при использовании пайплайна в другом окружении

// Репозитории
def gitlabLoginPasswordCredentials = 'gitlab-public'
def appRepositoryUrl = 'https://gitlab.com/greensight/ensi/devops/php-base-image.git'

// Параметры образа и реджистри
def registryLoginPasswordCredentials = 'dockerhub-gs'
def registryAddress = 'https://dockerhub.greensight.ru'
def registryName = 'dockerhub.greensight.ru'
def imageName = 'ensi-tech/php-base-image'
def fullImageName = "${registryName}/${imageName}"

// ===========================================================================//

properties([
    parameters([
        choice(name: 'PHP_VERSION', choices: ['8.4', '8.3', '8.2' ], description: 'Версия PHP, образ которой надо собрать')
    ]),
    buildDiscarder(logRotator (artifactDaysToKeepStr: '', artifactNumToKeepStr: '5', daysToKeepStr: '', numToKeepStr: '10')),
    disableConcurrentBuilds(),
])

node('docker-agent'){
    stage('Checkout') {
        git([url: appRepositoryUrl, branch: env.BRANCH_NAME, credentialsId: gitlabLoginPasswordCredentials])
    }
    stage('Build base') {
        def now = new Date()
        def currentDateStr = now.format("yyyy-MM-dd", TimeZone.getTimeZone('UTC'))

        def dockerTag = "${env.PHP_VERSION}-${env.BRANCH_NAME}-${currentDateStr}-${BUILD_NUMBER}"
        def ciDockerTag = "${env.PHP_VERSION}-ci-${env.BRANCH_NAME}-${currentDateStr}-${BUILD_NUMBER}"

        def fullImageNameWithTag = "${fullImageName}:${dockerTag}"
        def fullCIImageNameWithTag = "${fullImageName}:${ciDockerTag}"

        docker.withRegistry(registryAddress, registryLoginPasswordCredentials) {
            def baseImage = docker.build(fullImageNameWithTag, "--file ${PHP_VERSION}/base.dockerfile ${PHP_VERSION}")
            def ciImage = docker.build(fullCIImageNameWithTag, "--file ${PHP_VERSION}/ci.dockerfile --build-arg BASE_IMAGE=${fullImageNameWithTag} ${PHP_VERSION}")

            baseImage.push(dockerTag)
            ciImage.push(ciDockerTag)
        }

        currentBuild.displayName = "#${dockerTag}"
        currentBuild.description = [
            "Base Image : ${fullImageNameWithTag}",
            "CI Image : ${fullCIImageNameWithTag}",
        ].join("\n")
    }
}
