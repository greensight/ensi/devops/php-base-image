FROM composer:2.5 as composer

FROM dockerhub.greensight.ru/ensi-tech/php-base-image:8.1-master-2023mar4-1-swoole

RUN printf "# composer php cli ini settings\n\
date.timezone=UTC\n\
memory_limit=-1\n\
" > $PHP_INI_DIR/php-cli.ini

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN apk add --virtual .build-deps --no-cache --update autoconf file g++ gcc libc-dev make pkgconf re2c zlib-dev linux-headers && \
    apk add git npm bash && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    apk del .build-deps
