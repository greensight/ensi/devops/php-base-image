FROM php:8.3.7-fpm-alpine3.18

RUN apk add --virtual .build-deps --no-cache --update autoconf dpkg-dev dpkg file g++ gcc libc-dev make pkgconf re2c \
        zlib-dev=1.2.13-r1 freetype-dev=2.13.0-r5 libjpeg-turbo-dev=2.1.5.1-r3 libwebp-dev=1.3.2-r0 libpng-dev=1.6.44-r0 libzip-dev=1.9.2-r2 postgresql-dev librdkafka-dev=2.1.1-r0 \
        bash git ca-certificates && \
    apk add --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted \
        gnu-libiconv=1.17-r2 freetype=2.13.0-r5 libjpeg-turbo=2.1.5.1-r3 libwebp=1.3.2-r0 libpng=1.6.44-r0 libzip=1.9.2-r2 libpq=15.10-r0 librdkafka=2.1.1-r0 && \
    pecl install rdkafka-5.0.2 apcu-5.1.24 redis-6.1.0 swoole-6.0.0 && \
    docker-php-ext-configure gd \
        --with-freetype=/usr/include/ \
        --with-webp=/usr/include/ \
        --with-jpeg=/usr/include/ && \
    docker-php-ext-install pdo_pgsql gd zip opcache pcntl && \
    docker-php-ext-enable apcu pdo_pgsql redis rdkafka swoole && \
    apk del -f .build-deps && \
    pecl clear cache

COPY rootfs /

RUN cat /usr/local/share/ca-certificates/cert_ca_mintsifry.crt >> /etc/ssl/certs/ca-certificates.crt && \
    update-ca-certificates

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

WORKDIR /var/www
