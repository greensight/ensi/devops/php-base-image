# Базовый образ PHP-FPM для севрисов ensi на базе laravel #

В этом репозитории собраны конфигурации для сборки образов PHP-FPM разных версий и разных вариантов.
Каждая версия PHP находится в своей папке. В каждой папке есть несколько файлов *.dockerfile, по одному на каждый вариант.

На данный момент существуют варианты:
- 8.0 base
- 8.0 newrelic - базовый образ с установленным в него newrelic агентом
- 8.0 ci - образ для использования в ci пайплайне
- 8.1 base
- 8.1 newrelic - базовый образ с установленным в него newrelic агентом
- 8.1 ci - образ для использования в ci пайплайне

## Ручная сборка ##

```shell
cd 8.1
docker build -t base-tag --file base.dockerfile .
docker build -t newrelic-tag --build-arg BASE_IMAGE=base-tag --file newrelic.dockerfile .
docker build -t ci-tag --build-arg BASE_IMAGE=base-tag --file ci.dockerfile .
docker build -t swoole-tag --file swoole.dockerfile .
```

## Сборка через Jenkins ##

Необходимо создать обычный Multibranch Pipeline и в его параметрах указать что Jenkinsfile лежит по пути `.ci/ensi.jenkinsfile`
> при настроке сборки в другом окружении необходимо заменить или скопировать `.ci/ensi.jenkinsfile` и использовать уже новый файл 

После этого надо один раз запустить сборку и быстро ей выключить - грязхный хак чтобы дженкинс прочитал пайплайн и понял,
что в нём есть параметры.
Обновить страницу и отгрузить с параметрами.

Парамеры сборки:
- PHP_VERSION - версия php (папка в репозитории), образ которой надо пересобрать
- IMAGE_VARIANT - вариант образа (base, ci или newrelic)
- IMAGE_TAG_VERSION - суффикс для тега образа

Все эти параметры отражаются в тэге образа вот так:
`<PHP_VERSION>-<IMAGE_VARIANT>-<IMAGE_TAG_VERSION>`
т.е. если собрать образ спараметрами `8.0`, `newrelic` и `111`, то тэг будет иметь вид `8.0-newrelic-111`

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
